#!/usr/bin/env bash
for (( i=1; i <= 50; i++ ))
do
    curl -i -v -X POST -H "Content-Type: text/x-yaml" localhost:9999/start -d "
       apache$i:
           type: docker_container
           properties:
             image: httpd
             command: httpd-foreground
             port_bindings:
               - 500$i: 80
"
done
