#!/usr/bin/env bash
curl -i -v -X POST -H "Content-Type: text/x-yaml" localhost:9999/start -d "
   apache:
       type: docker_container
       properties:
         image: httpd
         command: httpd-foreground
         port_bindings:
           - 8080: 80
"
