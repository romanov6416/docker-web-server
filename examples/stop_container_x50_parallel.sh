#!/usr/bin/env bash
for (( i=1; i <= 50; i++ ))
do
    curl -i -v -X POST localhost:9999/stop/apache$i &
done
wait