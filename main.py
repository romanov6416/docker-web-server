#!/usr/bin/python3.6
import logging
import asyncio
from src import DockerServer

LOG = logging.getLogger(__name__)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    loop = asyncio.get_event_loop()
    ds = DockerServer(loop=loop)
    loop.run_until_complete(ds.start(port=9999))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        loop.run_until_complete(ds.stop())
    LOG.info("docker server is finished")
