import typing


class PortBinding:
    def __init__(self, external_ip, external_port, container_port):
        self.container_port = container_port
        self.external_port = external_port
        self.external_ip = external_ip


class ContainerRunParams:
    def __init__(self, name: str, image: str, command: str,
                 port_bindings: typing.List[PortBinding]):
        self.name = name
        self.image = image
        self.command = command
        self.port_bindings = port_bindings


class ContainerParams:
    def __init__(self, short_id: str, image: str, command: str,
                 created: str, status: str, port_bindings: list,
                 name: str):
        self.short_id = short_id
        self.name = name
        self.image = image
        self.command = command
        self.created = created
        self.status = status
        self.port_bindings = port_bindings
