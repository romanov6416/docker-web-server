import docker
import functools
import logging

import src.structures as struct

LOG = logging.getLogger(__name__)


def _safe_docker_connect(func):
    @functools.wraps(func)
    def wrapper(*arg, **kwargs):
        docker_client = docker.from_env()
        try:
            return func(docker_client, *arg, **kwargs)
        finally:
            docker_client.close()
    return wrapper


@_safe_docker_connect
def container_list(docker_client: docker.DockerClient):
    LOG.debug("requesting container list")
    out_container_list = [
        struct.ContainerParams(
                short_id=container.short_id,
                image=container.attrs["Config"]["Image"],
                command=container.attrs["Path"],
                created=container.attrs["Created"],
                status=container.status,
                port_bindings=[
                    struct.PortBinding(
                        external_ip=port_mapping["HostIp"] if port_mapping["HostIp"] else "0.0.0.0",
                        external_port=port_mapping["HostPort"],
                        container_port=container_port,
                    )
                    for container_port, port_mapping_list in container.attrs["HostConfig"]["PortBindings"].items()
                    for port_mapping in port_mapping_list
                    # if port_mapping and "HostPort" in port_mapping
                ],
                name=container.attrs["Name"],
        )
        for container in docker_client.containers.list()
    ]
    return out_container_list


@_safe_docker_connect
def stop_container(docker_client: docker.DockerClient, container_id: str):
    LOG.debug("finding container by id '%s'" % container_id)
    container = docker_client.containers.get(container_id)
    LOG.debug("stopping container by id '%s'" % container_id)
    container.stop()
    LOG.debug("removing container by id '%s'" % container_id)
    container.remove()


@_safe_docker_connect
def start_container(docker_client: docker.DockerClient,
                    container_params: struct.ContainerRunParams):
    LOG.debug("starting container '%s'" % container_params.name)
    container = docker_client.containers.run(
        image=container_params.image,
        command=container_params.command,
        detach=True,
        name=container_params.name,
        ports={
            "%s/tcp" % port_binding.container_port: port_binding.external_port
            for port_binding in container_params.port_bindings
        },
    )
    return container.id
