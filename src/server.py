import asyncio
import yaml
import concurrent.futures
import aiohttp
import aiohttp.web
import json
import logging

from . import adapter
import src.structures as struct

LOG = logging.getLogger(__name__)


class DockerServer:
    def __init__(self, loop: asyncio.AbstractEventLoop = None):
        LOG.debug("initializing docker server ")
        self._app = aiohttp.web.Application()
        self._loop = loop if loop else asyncio.get_event_loop()
        self._server = None  # type: asyncio.base_events.Server
        self._app.router.add_post("/start", self._on_post_start)
        self._app.router.add_post("/stop/{identifier}", self._on_post_stop)
        self._app.router.add_get("/list", self._on_get_list)
        self._executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)

    async def start(self, host: str = None, port: int = None):
        LOG.info("starting server")
        if self._server:
            raise Exception("already started")
        self._server = await self._loop.create_server(
            protocol_factory=self._app.make_handler(),
            host=host if host else "localhost",
            port=port if port else 80,
        )  # type: asyncio.base_events.Server
        LOG.info("ready")

    async def stop(self):
        LOG.info("stopping server")
        if not self._server:
            raise Exception("already stopped")
        self._server.close()
        await self._server.wait_closed()

    def _run_in_executor(self, func, *args):
        return self._loop.run_in_executor(self._executor, func, *args)

    async def _on_post_start(self, request: aiohttp.web.Request):
        try:
            # load data
            payload_str = await request.text()
            payload_dict = yaml.load(payload_str)
            # check format
            if len(payload_dict) != 1:
                raise Exception("support only one container description")
            name, params = payload_dict.popitem()
            container_params = struct.ContainerRunParams(
                name=name,
                image=params["properties"]["image"],
                command=params["properties"]["command"],
                port_bindings=[
                    struct.PortBinding(
                        external_ip="0.0.0.0",
                        external_port=external_port,
                        container_port=container_port
                    )
                    for port_pair in params["properties"]["port_bindings"]
                    for external_port, container_port in port_pair.items()
                ],
            )
        except Exception as e:
            str_error = "fail to parse request: %s" % e
            LOG.error(str_error)
            return aiohttp.web.Response(status=400, reason=str_error)
        try:
            container_id = await self._run_in_executor(
                adapter.start_container, container_params,
            )
        except Exception as e:
            str_error = "fail to start container: %s" % e
            LOG.error(str_error)
            return aiohttp.web.Response(status=400, reason=str_error)
        return aiohttp.web.Response(
            status=200,
            body=str(container_id),
        )

    async def _on_post_stop(self, request: aiohttp.web.Request):
        container_id = request.match_info["identifier"]
        try:
            await self._run_in_executor(adapter.stop_container, container_id)
        except Exception as e:
            str_error = "fail to stop container with id '%s': %s" % (container_id, e)
            LOG.error(str_error)
            return aiohttp.web.Response(status=400, reason=str_error)
        return aiohttp.web.Response(status=200)

    async def _on_get_list(self, request: aiohttp.web.Request):
        try:
            container_params_list = await self._run_in_executor(adapter.container_list)
            str_response_content = json.dumps([
                {
                    "CONTAINER_ID": container_params.short_id,
                    "IMAGE": container_params.image,
                    "COMMAND": container_params.command,
                    "CREATED": container_params.created,
                    "STATUS": container_params.status,
                    "PORTS": [
                        "%s:%s->%s" % (
                            port_binding.external_ip,
                            port_binding.external_port,
                            port_binding.container_port
                        )
                        for port_binding in container_params.port_bindings
                    ],
                    "NAMES": container_params.name,
                }
                for container_params in container_params_list
            ])
            return aiohttp.web.json_response(
                body=str_response_content,
                status=200,
            )
        except Exception as e:
            str_error = "fail to get container list: %s" % e
            LOG.error(str_error)
            return aiohttp.web.Response(status=400, reason=str_error)


